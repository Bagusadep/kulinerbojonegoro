<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Konfigurasi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
// Listing
	public function listing()
	{
		$query = $this->db->get('konfigurasi');
		return $query->row();
	}

// Edit

	public function edit($data)
	{
		$this->db->where('id_konfigurasi', $data['id_konfigurasi']);
		$this->db->update('konfigurasi', $data);
	}
// load menu kategori kuliner
	public function nav_kuliner()
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->group_by('kuliner.id_kategori');
		$this->db->order_by('kategori.urutan', 'ASC');
		$query = $this->db->get();
		return $query->result(); 
	}

}

/* End of file Konfigurasi_model */
/* Location: ./application/models/Konfigurasi_model */