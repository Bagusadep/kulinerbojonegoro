<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuliner_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing di ambil dari class Kuliner.php/ Listing all kuliner
	public function listing()
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->order_by('id_kuliner', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

// listing Home
	public function home()
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->order_by('id_kuliner', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

// read kuliner
	public function read($id_kuliner)
	{

		$query = "SELECT * FROM kuliner JOIN kategori USING(id_kategori) WHERE id_kuliner=" . $id_kuliner;

		// $this->db->select('kuliner.*,
		// 				kategori.nama_kategori,
		// 				kategori.slug_kategori');
		// $this->db->from('kuliner');
		// // JOIN database
		// $this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// // END JOIN

		// $this->db->order_by('id_kuliner', 'desc');
		// $query = $this->db->get()

		return $this->db->query($query)->row_array();
	}
	
// data kuliner
	public function kuliner($limit,$start)
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->group_by('kuliner.id_kuliner');
		$this->db->order_by('id_kuliner', 'desc');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		return $query->result();
	}

// total kuliner
public function total_kuliner()
{
	$this->db->select('COUNT(*) AS total');
	$this->db->from('kuliner');
	$query = $this->db->get();
	return $query->row();
}

// data kategori
	public function kategori($id_kategori, $limit,$start)
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->where('kuliner.id_kategori', $id_kategori);
		$this->db->group_by('kuliner.id_kuliner');
		$this->db->order_by('id_kuliner', 'desc');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		return $query->result();
	}

// total kategori 
public function total_kategori($id_kategori)
{
	$this->db->select('COUNT(*) AS total');
	$this->db->from('kuliner');
	$this->db->where('id_kategori', $id_kategori);
	$query = $this->db->get();
	return $query->row();
}

// Listing kategori
	public function listing_kategori()
	{
		$this->db->select('kuliner.*,
						kategori.nama_kategori,
						kategori.slug_kategori');
		$this->db->from('kuliner');
		// JOIN database
		$this->db->join('kategori', 'kategori.id_kategori = kuliner.id_kategori', 'left');
		// END JOIN
		$this->db->group_by('kuliner.id_kategori');
		$this->db->order_by('id_kuliner', 'desc');
		$query = $this->db->get();
		return $query->result();
	}


// Detail Kuliner
	public function detail($id_kuliner)
	{
		$this->db->select('*');
		$this->db->from('kuliner');
		$this->db->where('id_kuliner',$id_kuliner);
		$this->db->order_by('id_kuliner', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

// Login Kuliner
	public function login($kulinername, $password)
	{
		$this->db->select('*');
		$this->db->from('kuliner');
	 	$this->db->where(array(	'kulinername'	=> $kulinername,
	 							'password'	=> SHA1($password)));
		$this->db->order_by('id_kuliner', 'desc');
		$query = $this->db->get();
		return $query->row();
	}


	// Tambah
	public function tambah($data)
	{
		$this->db->insert('kuliner', $data);
	}

	// Edit
	public function edit($data)
	{
		$this->db->where('id_kuliner', $data['id_kuliner']);
		$this->db->update('kuliner',$data);
	}

	// Delete 
	public function delete($data)
	{
		$this->db->where('id_kuliner', $data['id_kuliner']);
		$this->db->delete('kuliner',$data);
	}
}

/* End of file Kuliner_model.php */
/* Location: ./application/models/Kuliner_model.php */