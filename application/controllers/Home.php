<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ini halaman utamanya gaes
class Home extends CI_Controller {

	// load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kuliner_model');
		$this->load->model('kategori_model');

			
	}

	// halaman utama 
	public function index()
	{
		$site 		= $this->konfigurasi_model->listing();
		$kategori 	= $this->konfigurasi_model->nav_kuliner();
		$kuliner 	= $this->kuliner_model->home();

		$data 		= array(	'title'		 	=> $site->namaweb, 
								'website'		=> $site->website,
								'site'			=> $site,
								'kuliner'		=> $kuliner,
								'kategori'		=> $kategori, 
								'isi'			=> 'home/list'
		 );
		$this->load->view('layout/wrapper' , $data, FALSE);
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */