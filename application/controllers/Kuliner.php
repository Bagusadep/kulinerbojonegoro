<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuliner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kuliner_model');
		$this->load->model('kategori_model');
	}

	public function index()
	{
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori		= $this->kuliner_model->listing_kategori();
		// Ambil data total
		$total					= $this->kuliner_model->total_kuliner();
		// paginasi start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'kuliner/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_number'] 	= TRUE;
		$config['per_page'] 		= 12;
		$config['uri_segment'] 		= 3;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_link'] 		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link'] 		= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close'] 	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['first_url'] 		= base_url().'/kuliner/';

		
		$this->pagination->initialize($config);

		// Ambil Data Kulinner
		
		$page 		=($this->uri->segment(3)) ? ($this->uri->segment(3)-1) * $config['per_page']:0;
		$kuliner 	=$this->kuliner_model->kuliner($config['per_page'],$page);
			// paginasi end 

		$data = array( 'title'				=> 'Aneka '.$site->namaweb,
						'site'				=> $site,
						'listing_kategori'	=> $listing_kategori,
						'kuliner'			=> $kuliner,
						'pagin'				=> $this->pagination->create_links(),
						'isi'				=> 'kuliner/list'
				);

		$this->load->view('layout/wrapper', $data, FALSE);
	}

// listing kategori
public function kategori($slug_kategori)
	{
		// kategori detail
		$kategori 				= $this->kategori_model->read($slug_kategori);
		$id_kategori			= $kategori->id_kategori;
		// Data Global
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori		= $this->kuliner_model->listing_kategori();
		// Ambil data total
		$total					= $this->kuliner_model->total_kategori($id_kategori);
		// paginasi start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'kuliner/kategori/'.$slug_kategori.'/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_number'] 	= TRUE;
		$config['per_page'] 		= 12;
		$config['uri_segment'] 		= 5;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_link'] 		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link'] 		= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close'] 	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['first_url'] 		= base_url().'/kuliner/kategori'.$slug_kategori;

		
		$this->pagination->initialize($config);

		// Ambil Data Kulinner
		
		$page 		=($this->uri->segment(5)) ? ($this->uri->segment(5)-1) * $config['per_page']:0;
		$kuliner 	=$this->kuliner_model->kategori($id_kategori, $config['per_page'],$page);
			// paginasi end 

		$data = array( 'title'				=> 'Kuliner '.$site->namaweb,
						'site'				=> $site,
						'listing_kategori'	=> $listing_kategori,
						'kuliner'			=> $kuliner,
						'pagin'				=> $this->pagination->create_links(),
						'isi'				=> 'kuliner/list'
				);

		$this->load->view('layout/wrapper', $data, FALSE);
	}


// detail
	public function detail($nama_kuliner)
	{

		$site 		= $this->konfigurasi_model->listing();
		$kuliner 	= $this->kuliner_model->read($nama_kuliner);
		$id_kuliner	= $kuliner['id_kuliner'];
		
		$data = array( 'title'				=> $kuliner['nama_kuliner'],
						'site'				=> $site,
						'kuliner'			=> $kuliner,
						'isi'				=> 'kuliner/detail'
				);
		$this->load->view('layout/wrapper', $data, FALSE);
	}


}

/* End of file Kuliner.php */
/* Location: ./application/controllers/Kuliner.php */