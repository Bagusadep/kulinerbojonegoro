<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuliner extends CI_Controller {

	// Load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kuliner_model');
		$this->load->model('kategori_model');
		// Proteksi halaman
		$this->simple_login->cek_login();
	}

	// Data Kuliner
	public function index()
	{
		$kuliner = $this->kuliner_model->listing();

		$data = array(	'title'			=> 'Data Kuliner',
						'kuliner'		=> $kuliner,
						'isi'			=> 'admin/kuliner/list'
					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	// Tambah Kuliner 
	public function tambah()
	{
		// Ambil data kategori
		$kategori =$this->kategori_model->listing();
		// Validasi Input
		$valid = $this->form_validation;

		$valid->set_rules('nama_kuliner','Nama lengkap','required',
			array(	'required'		=> '%s harus diisi'));

		$valid->set_rules('kode_kuliner','Kode Kuliner','required',
			array(	'required'		=> '%s harus diisi',
					'is_unique'		=> '%s sudah ada. Buat Kode Kuliner Baru'));

	 

		if($valid->run()) {

			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['max_size']  		= '2400';
			$config['max_width'] 		= '2024';
			$config['max_height']		= '2024';

			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
				
				
		// END Validasi

		$data = array(	'title'		=>	'Tambah Kuliner',
						'kategori'	=>	$kategori,
						'error'		=> $this->upload->display_errors(),
						'isi'		=>	'admin/kuliner/tambah'
					);

		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{

		$upload_gambar = array('upload_data' => $this->upload->data());

		// create thumbnail gambar
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
		// lokasi folder thumbnail
		$config['new_image']		= './assets/upload/image/thumbs/';
		$config['create_thumb'] 	= TRUE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']         	= 250; //pixel
		$config['height']       	= 250; //pixel
		$config['thumb_marker']		= '';

$this->load->library('image_lib', $config);

$this->image_lib->resize();
		// end create thumbnail

		$i = $this->input;
		// slug kuliner
		$slug_kuliner = url_title($this->input->post('nama_kuliner').'-'.$this->input->post('kode_kuliner'), 'dash', TRUE);
		$data = array(	
						'nama_kuliner'		=> $i->post('nama_kuliner'),
						'id_kategori'		=> $i->post('id_kategori'),
						'kode_kuliner'		=> $i->post('kode_kuliner'),
						'slug_kuliner'		=> $slug_kuliner,
						'keterangan'		=> $i->post('keterangan'),
						'harga'				=> $i->post('harga'),
						'gambar'			=> $upload_gambar['upload_data']['file_name']
					);
		$this->kuliner_model->tambah($data);
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/kuliner'),'refresh');
		}}
	// End Masuk database
		$data = array(	'title'		=>	'Tambah Kuliner',
						'kategori'	=>	$kategori,
						'isi'		=>	'admin/kuliner/tambah'
					);

		$this->load->view('admin/layout/wrapper', $data, FALSE);

	}

	// Edit Kuliner 
	public function edit($id_kuliner)
	{
		$kuliner = $this->kuliner_model->detail($id_kuliner);

		$kategori =$this->kategori_model->listing();
		
		// Validasi Input
		$valid = $this->form_validation;

		$valid->set_rules('nama_kuliner','Nama lengkap','required',
			array(	'required'		=> '%s harus diisi'));

		$valid->set_rules('keterangan','keterangan','required',
			array(	'required'		=> '%s harus diisi'));

		$valid->set_rules('harga','Harga','required',
			array(	'required'		=> '%s harus diisi'));

		$valid->set_rules('kode_kuliner','Kode Kuliner','required',
			array(	'required'		=> '%s harus diisi',
					));
	

		if($valid->run()) {
			// check jika gambar diganti
			if (!empty($_FILES['gambar']['name'])) {

			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['max_size']  		= '2400';
			$config['max_width'] 		= '2024';
			$config['max_height']		= '2024';

			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
				
				
		// END Validasi

		$data = array(	'title'		=>	'Edit Kuliner: '.$kuliner->nama_kuliner,
						'kategori'	=>	$kategori,
						'kuliner'	=> 	$kuliner,
						'error'		=> 	$this->upload->display_errors(),
						'isi'		=>	'admin/kuliner/edit'
					);

		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{

		$upload_gambar = array('upload_data' => $this->upload->data());

		// create thumbnail gambar
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
		// lokasi folder thumbnail
		$config['new_image']		= './assets/upload/image/thumbs/';
		$config['create_thumb'] 	= TRUE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']         	= 250; //pixel
		$config['height']       	= 250; //pixel
		$config['thumb_marker']		= '';

$this->load->library('image_lib', $config);

$this->image_lib->resize();
		// end create thumbnail

		$i = $this->input;
		// slug kuliner
		$slug_kuliner = url_title($this->input->post('nama_kuliner').'-'.$this->input->post('kode_kuliner'), 'dash', TRUE);
		$data = array(	
						'id_kuliner'		=> $id_kuliner,
						'nama_kuliner'		=> $i->post('nama_kuliner'),
						'id_kategori'		=> $i->post('id_kategori'),
						'kode_kuliner'		=> $i->post('kode_kuliner'),
						'slug_kuliner'		=> $slug_kuliner,
						'keterangan'		=> $i->post('keterangan'),
						'harga'				=> $i->post('harga'),
						'gambar'			=> $upload_gambar['upload_data']['file_name']
					);
		$this->kuliner_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/kuliner'),'refresh');
		}}else{

			// edit kuliner tanpa ganti gambar
		$i = $this->input;
		// slug kuliner
		$slug_kuliner = url_title($this->input->post('nama_kuliner').'-'.$this->input->post('kode_kuliner'), 'dash', TRUE);
		$data = array(	
						'id_kuliner'		=> $id_kuliner,
						'nama_kuliner'		=> $i->post('nama_kuliner'),
						'id_kategori'		=> $i->post('id_kategori'),
						'kode_kuliner'		=> $i->post('kode_kuliner'),
						'slug_kuliner'		=> $slug_kuliner,
						'keterangan'		=> $i->post('keterangan'),
						'harga'				=> $i->post('harga')
						//'gambar'			=> $upload_gambar['upload_data']['file_name']
					);
		$this->kuliner_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/kuliner'),'refresh');

		}}
	// End Masuk database
		$data = array(	'title'		=>	'Edit Kuliner: '.$kuliner->nama_kuliner,
						'kategori'	=>	$kategori,
						'kuliner'	=>	$kuliner,
						'isi'		=>	'admin/kuliner/edit'
					);

		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}


	// Delete kuliner
	public function delete($id_kuliner)
	{
		// Proses hapus gambar
		$kuliner = $this->kuliner_model->detail($id_kuliner);
		unlink('./assets/upload/image/'.$kuliner->gambar);
		unlink('./assets/upload/image/thumbs/'.$kuliner->gambar);	
		// End proses hapus gambar
		$data = array('id_kuliner' => $id_kuliner);
		$this->kuliner_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/kuliner'),'refresh');
	}

}

/* End of file kuliner.php */
/* Location: ./application/controllers/admin/kuliner.php */

