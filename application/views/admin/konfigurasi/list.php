<?php
// Notifikasi
if($this->session->flashdata('sukses')) {
  echo '<p class="alert alert-success">';
  echo $this->session->flashdata('sukses');
  echo '</div>';
  // Nanti itu muncul belakangan
}
?>

<?php 
// notifikasi error
if (isset($error)) {
  echo '<p class="alert alert-warning">';
  echo $error;
  echo '</p>';
}
// motifikasi error
echo validation_errors('<div class="alert alert-warning">','</div>');

//from open
echo form_open_multipart(base_url('admin/konfigurasi'), ' class="form-horizontal"');
 ?>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Nama Website</label>
  <div class="col-md-5">
    <input type="text" name="namaweb" class="form-control" placeholder="Nama Website" value="<?php echo $konfigurasi->namaweb ?>" required>
  </div>
</div>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Deskripsi Website</label>
  <div class="col-md-5">
    <input type="text" name="website" class="form-control" placeholder="Deskripsi Website" value="<?php echo $konfigurasi->website ?>" required>
  </div>
</div>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Email</label>
  <div class="col-md-5">
    <input type="Email" name="email" class="form-control" placeholder="Email Website" value="<?php echo $konfigurasi->email ?>" required>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label"></label>
  <div class="col-md-5">
    <button class="btn btn-success btn-lg" name="submit" type="submit">
    <i class="fa fa-save"></i> Simpan
    </button>

    <button class=" btn btn-info btn-lg" name="reset" type="reset">
    <i class="fa fa-times"></i> Reset
    </button>   
  </div>
</div>

 <?php echo form_close(); ?> 