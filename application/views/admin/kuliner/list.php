<p>
	<a href="<?php echo base_url('admin/kuliner/tambah') ?>" class="btn btn-success btn-lg">
		<i class="fa fa-plus"></i> Tambah Baru
	</a>
</p>
<!-- ini kodingan muncul di toko/admin/kuliner -->
<?php
// Notifikasi
if($this->session->flashdata('sukses')) {
	echo '<p class="alert alert-success">';
	echo $this->session->flashdata('sukses');
	echo '</div>';
	// Nanti itu muncul belakangan
}
?>

<table class="table table-bordered" id="example1">
	<thead> 
		<tr>
			<th>NO</th>
			<th>GAMBAR</th>
			<th>NAMA</th>
			<th>SLUG</th>
			<th>KATEGORI</th>
			<th>KETERANGAN</th>
			<th>HARGA</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach($kuliner as $kuliner) { ?>
		<tr> 
			<td><?php echo $no ?></td>
			<td>
				<img src="<?php echo base_url('assets/upload/image/thumbs/'.$kuliner->gambar) ?>" class="img-responsive img-thumbnail" width="60">
			</td>
			<td><?php echo $kuliner->nama_kuliner ?></td>
			<td><?php echo $kuliner->slug_kuliner ?></td>
			<td><?php echo $kuliner->nama_kategori ?></td>
			<td><?php echo $kuliner->keterangan ?></td>
			<td><?php echo number_format($kuliner->harga,'0',',','.') ?></td>
			<td>
				<a href="<?php echo base_url('admin/kuliner/edit/'.$kuliner->id_kuliner) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit </a>

				<?php include('delete.php') ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>	
</table>
