<?php 
// notifikasi error
if (isset($error)) {
  echo '<p class="alert alert-warning">';
  echo $error;
  echo '</p>';
}
// motifikasi error
echo validation_errors('<div class="alert alert-warning">','</div>');

//from open
echo form_open_multipart(base_url('admin/kuliner/edit/'. $kuliner->id_kuliner), ' class="form-horizontal"');
 ?>
<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Nama Kuliner</label>
  <div class="col-md-5">
    <input type="text" name="nama_kuliner" class="form-control" placeholder="Nama Kuliner" value="<?php echo $kuliner->nama_kuliner ?>" required>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Kategori Kuliner</label>
  <div class="col-md-5">
    <select name="id_kategori" class="form-control">
       <?php foreach ($kategori as $kategori) { ?>
        <option value="<?php echo $kategori->id_kategori ?>" <?php if($kuliner->id_kategori==$kategori->id_kategori) { echo "selected"; }  ?>> 
          <?php echo $kategori->nama_kategori ?>
        </option>
      <?php } ?>
    </select>
  </div>
</div>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Kode Kuliner</label>
  <div class="col-md-5">
    <input type="text" name="kode_kuliner" class="form-control" placeholder="Kode Kuliner" value="<?php echo $kuliner->kode_kuliner ?>" required>
  </div>
</div>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Keterangan</label>
  <div class="col-md-10">
    <textarea  name="keterangan" class="form-control" placeholder="keterangan" id="editor"><?php echo $kuliner->keterangan ?></textarea>
  </div>
</div>

<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Harga</label>
  <div class="col-md-10">
    <input type="number" name="harga" class="form-control" placeholder="Harga" value="<?php echo $kuliner->harga ?>" required>
 </div>
</div>


<div class="form-group form-group-lg" >
  <label class="col-md-2 control-label">Upload Gambar Kuliner</label>
  <div class="col-md-10">
    <input type="file" name="gambar" class="form-control">
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label"></label>
  <div class="col-md-5">
    <button class="btn btn-success btn-lg" name="submit" type="submit">
    <i class="fa fa-save"></i> Simpan
    </button>

    <button class=" btn btn-info btn-lg" name="reset" type="reset">
    <i class="fa fa-times"></i> Reset
    </button>   
  </div>
</div>

 <?php echo form_close(); ?> 