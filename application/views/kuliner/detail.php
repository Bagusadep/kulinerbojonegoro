<!-- breadcrumb -->
<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
<a href="<?php echo base_url() ?>" class="s-text16">
Home
<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
</a>
  
<a href="<?php echo base_url('kuliner') ?>" class="s-text16">
Kuliner
<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
</a>

<span class="s-text17">
	<?php echo $title  ?>
</span>
</div>

<!-- Product Detail -->
<div class="container bgwhite p-t-35 p-b-80">
<div class="flex-w flex-sb">
<div class="w-size13 p-t-30 respon5">
	<div class="wrap-slick3 flex-sb flex-w">
		<div class="wrap-slick3-dots"></div>

		<div class="slick3">
			<div class="item-slick3" data-thumb="<?php echo base_url('assets/upload/image/thumbs/'.$kuliner['gambar']) ?>">
				<div class="wrap-pic-w">
					<img src="<?php echo base_url('assets/upload/image/'.$kuliner['gambar']) ?>" alt="<?php echo $kuliner['nama_kuliner'] ?>">
				</div>
			</div>

			
		</div>
	</div>
</div>

<div class="w-size14 p-t-30 respon5">
	<h4 class="product-detail-name m-text16 p-b-13">
		<?php echo $title ?>
	</h4>

<br>
		<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
			Kisaran Harga
			<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
			<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
		</h5>
	<span class="m-text17">
		IDR <?php echo number_format($kuliner['harga'],'0',',','.') ?>
	</span>


	
	<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
		<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
			Deskripsi
			<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
			<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
		</h5>

	<p class="s-text8 p-t-10">
		<?php echo $kuliner['keterangan'] ?>
	</p>

		
	</div>
</div>
</div>
</div>


</section>