<!-- Title Page -->
<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url() ?>assets/fashe/images/heading-pages-02.jpg);">
<h2 class="l-text2 t-center">
	<?php echo $title ?>
</h2>
<p class="m-text13 t-center">
	
</section>


<!-- Content page -->
<section class="bgwhite p-t-55 p-b-65">
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
			<div class="leftbar p-r-20 p-r-0-sm">
				<!--  -->
				<h4 class="m-text14 p-b-7">
					Kategori Kuliner
				</h4>

				<ul class="p-b-54">
					<?php foreach($listing_kategori as $listing_kategori) { ?>
					<li class="p-t-4">
						<a href="<?php echo base_url('kuliner/kategori/'.$listing_kategori->slug_kategori) ?>" class="s-text13 active1">
							<?php echo $listing_kategori->nama_kategori; ?>
						</a>
					</li> 
				<?php } ?>
				</ul>

			</div>
		</div>

		<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
			
			<!-- Product -->
			<div class="row">
				<?php foreach($kuliner as $kuliner) { ?>
				<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">

					<!-- Block2 -->
					<div class="block2">
						<div class="block2-img wrap-pic-w of-hidden pos-relative">
							<img src="<?php echo base_url('assets/upload/image/thumbs/'.$kuliner->gambar) ?>" alt="<?php echo $kuliner->nama_kuliner ?>" width="300px" height="300px">

							<div class="block2-overlay trans-0-4">
								

								
							</div>
						</div>

						<div class="block2-txt p-t-20">
							<a href="<?php echo base_url('kuliner/detail/'.$kuliner->id_kuliner) ?>" class="block2-name dis-block s-text3 p-b-5">
								<?php echo $kuliner->nama_kuliner ?>
							</a>

							<span class="block2-price m-text6 p-r-5">
								IDR <?php echo number_format($kuliner->harga,'0',',','.') ?>
							</span>
						</div>
					</div>
				</div>
			<?php } ?>

		</div>
				

			<!-- Pagination -->
			<div class="pagination flex-m flex-w p-t-26">
				<?php echo $pagin ?>
		</div>
	</div>
</div>
</section>

