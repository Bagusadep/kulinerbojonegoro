<?php 
// ambil data menu dari konfigurasi model
$nav_kuliner    = $this->konfigurasi_model->nav_kuliner();
?>



<div class="wrap_header">
        <!-- Logo -->
        <a href="<?php echo base_url() ?>" class="logo">
          <img src="<?php echo base_url('assets/upload/image/'.$site->logo) ?>" alt="<?php echo $site->namaweb ?>">
        </a>

        <!-- Menu -->
        <div class="wrap_menu">
          <nav class="menu">
            <ul class="main_menu">
              
              <!-- Home -->
              <li>
                <a href="<?php echo base_url() ?>">Home</a>
              </li>

              <!-- Menu Kuliner -->
              <li>
                <a href="<?php echo base_url('kuliner') ?>">Kuliner</a>
                <ul class="sub_menu">
                 <!--  -->
                  <?php foreach($nav_kuliner as $nav_kuliner) { ?>
                  <li><a href="<?php echo base_url('kuliner/kategori/'.$nav_kuliner->slug_kategori) ?>">   
                  <?php echo $nav_kuliner->nama_kategori ?>
                </a></li>
              <?php } ?>
                </ul>
              </li>

            
             
            </ul>
          </nav>
        </div>

        
    </div>
  </header>